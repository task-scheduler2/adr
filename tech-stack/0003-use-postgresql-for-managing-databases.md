# Use PostgreSQL for managing databases

## Context and Problem Statement

Which database managment system whould we use?

## Considered Options

* SQLite
* PostgreSQL

## Decision Outcome

Chosen option: PostgreSQL, because

* It's more advanced
* It's more secure

