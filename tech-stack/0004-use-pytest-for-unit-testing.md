# Use PostgreSQL for managing databases

## Context and Problem Statement

Which testing framework whould we use?

## Considered Options

* Pytest
* PyUnit
* TestProject
* Nose2

## Decision Outcome

Chosen option: Pytest, because

* Team is most familiar with it
* It supports higher level testsas well as unit tests

