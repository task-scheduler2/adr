# Use Flask as web framework

## Context and Problem Statement

Which framework whould we use?

## Considered Options

* Flask
* Django

## Decision Outcome

Chosen option: Flask, because

* It is a framework team knows best
* It works well with small projects

