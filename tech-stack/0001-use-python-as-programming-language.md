# Use Python as programming language

## Context and Problem Statement

Which language to write the project in?

## Considered Options

* Python 3

## Decision Outcome

Chosen option: Python 3 because

* It is a programming language team knows best

