# Use Black for Code formatting

## Context and Problem Statement

Uniform code formatting should be enforced. How?

## Considered Options

* Black
* Other code formatters
* Formatting the code without automatic tools

## Decision Outcome

Chosen option: Black, because

* Team is familiar with it
* It saves time and energy of the developers while still ensuring uniform format 
