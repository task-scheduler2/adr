# Use MADR as ADR template

## Context and Problem Statement

To properly document decisions in this project architectural decision records were needed. Which template would be the best?

## Considered Options

* [MADR](https://adr.github.io/madr/) 3.0.0 – The Markdown Any Decision Records
* [Michael Nygard's template](http://thinkrelevance.com/blog/2011/11/15/documenting-architecture-decisions) – The first incarnation of the term "ADR"
* [Sustainable Architectural Decisions](https://www.infoq.com/articles/sustainable-architectural-design-decisions) – The Y-Statements
* Other templates listed at <https://github.com/joelparkerhenderson/architecture_decision_record>

## Decision Outcome

Chosen option: "MADR 3.0.0", because

* It lists considered options which is useful
* It's structure is simple and easy to understand for anyone
* No date or deciders need to be listed as those information will be visible on GIT