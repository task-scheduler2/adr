# Use GitLab for Git hosting

## Context and Problem Statement

A GIT repository should be hosted somewhere. Which site to choose? 

## Considered Options

* GitHub
* GitLab

## Decision Outcome

Chosen option: GitLab, because

* It will allow the team to grow accustomed to it
* It will allow me the team to practice CI/CD more
